import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';
import { HttpResponse } from '@angular/common/http';
import { takeUntil, take } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-update-beer',
  templateUrl: './update-beer.component.html',
  styleUrls: ['./update-beer.component.scss']
})
export class UpdateBeerComponent implements OnInit, OnDestroy {
  destroy$: Subject<boolean> = new Subject<boolean>();
  beerId;
  beer = {};

  constructor(private activatedRoute: ActivatedRoute,
    private apiService: ApiService,
    @Inject(DOCUMENT) private _document: Document
    ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.beerId = params['id'];
      console.log('Url Id: ',this.beerId);
    })

    this.apiService.sendSingleGetRequest(this.beerId).pipe(takeUntil(this.destroy$)).subscribe((res: HttpResponse<any>)=>{
      console.log(res.body)
      this.beer = res.body;
    });

  }

  updateBeer(name, style, color, status, ibu, abv, numBottles, image, description){
    let data = {
      "name": name.value || "",
      "style": style.value || "",
      "status": status.value || "",
      "color": color.value || "",
      "ibu": ibu.value || "",
      "abv": abv.value || "",
      "quantity": numBottles.value || "",
      "picture": image.value || "",
      "description": description.value || ""
    };

    this.apiService.updateBeer(this.beerId, data);
    this._document.defaultView.location.reload();
  }

  //not really sure if this is needed, I would much rather be able to keep track of all my beers
  //maybe add an 'are you sure?' modal to this at some point?
  deleteBeer(beerId: number) {
    this.apiService.deleteBeer(this.beerId);

    //maybe have this go back to the main admin page instead of just reloading
    this._document.defaultView.location.reload();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    //unsubscribe from the subject itself:
    this.destroy$.unsubscribe();
  }
}
