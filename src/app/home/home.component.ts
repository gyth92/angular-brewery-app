import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from '../api.service';
import { HttpResponse } from '@angular/common/http';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit, OnDestroy {
  destroy$: Subject<Boolean> = new Subject<Boolean>();
  response = [];
  toShow = []

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.sendGetRequest().pipe(takeUntil(this.destroy$)).subscribe((res: HttpResponse<any>)=>{   
      this.response = res.body;
      this.sortBeers(this.response);
    })
  }

  sortBeers(res) {
   for (let beer of res){
     if(beer.status == 'fermenting' || beer.status == 'planning') {
       this.beerColor(beer);
      this.toShow.push(beer);
    }  
   }
  }

  beerColor(beer) {
    if(beer.color == 'straw') {
      beer.showColor = '#FEF23B';
    } else if(beer.color == 'golden') {
      beer.showColor = '#F09D0B'
    } else if(beer.color == 'orange') {
      beer.showColor = '#CF520A';
    } else if(beer.color == 'amber') {
      beer.showColor = '#8B0503';
    } else if(beer.color == 'brown') {
      beer.showColor = '#53190D'
    } else if(beer.color == 'dark') {
      beer.showColor = '#210302';
    } else if(beer.color == 'black') {
      beer.showColor = '#0B0905';
    }
  }
  
  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

}





