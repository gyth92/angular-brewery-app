import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { LoginService } from '../login.service';
import { ApiService } from '../api.service';
import { HttpResponse } from '@angular/common/http';
import { takeUntil, take } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit, OnDestroy {
  destroy$: Subject<boolean> = new Subject<boolean>();
  beers = [];
  loggedIn = false;

  constructor(private loginService: LoginService,
    private apiService: ApiService,
    @Inject(DOCUMENT) private _document: Document)
    { }

  ngOnInit() {
    this.apiService.sendGetRequest().pipe(takeUntil(this.destroy$)).subscribe((res: HttpResponse<any>)=> {
      this.beers = res.body;
    });
  }

  submitLogin(usrName, pass) {
    // this.loggedIn = this.loginService.handleLogin(usrName.value, pass.value)
    this.loggedIn = true;
  }

  createBeer(name, style, color, status, ibu, abv, numBottles, image, description){
    let data = {
      "name": name.value || "",
      "style": style.value || "",
      "status": status.value || "",
      "color": color.value || "",
      "ibu": ibu.value || "",
      "abv": abv.value || "",
      "quantity": numBottles.value || "",
      "picture": image.value || "",
      "description": description.value || ""
    };

    this.apiService.createBeer(data);
    this._document.defaultView.location.reload();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    //unsubscribe from the subject itself:
    this.destroy$.unsubscribe();
  }
}
