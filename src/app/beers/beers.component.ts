import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from '../api.service';
import { HttpResponse } from '@angular/common/http';
import { takeUntil, take } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-beers',
  templateUrl: './beers.component.html',
  styleUrls: ['./beers.component.scss']
})
export class BeersComponent implements OnInit, OnDestroy {
  destroy$: Subject<boolean> = new Subject<boolean>();
  beers = [];
  toShow = [];

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.sendGetRequest().pipe(takeUntil(this.destroy$)).subscribe((res: HttpResponse<any>)=>{  
      this.beers = res.body;
      this.sortBeers(this.beers);
    });

    // this.apiService.sendPostRequest();
    // this.apiService.updateBeer(9);
  }

  sortBeers(res) {
    for (let beer of res){
      if(beer.status == 'conditioning' || beer.status == 'complete') {
       this.toShow.push(beer);
     }  
    }
   }

  ngOnDestroy() {
    this.destroy$.next(true);
    //unsubscribe from the subject itself:
    this.destroy$.unsubscribe();
  }

}


// public firstPage() {
  //   this.products = [];
  //   this.apiService.sendGetRequestToUrl(this.apiService.first).pipe(takeUntil(this.destroy$)).subscribe((res: HttpResponse<any>) => {
  //     console.log(res);
  //     this.products = res.body;
  //   })
  // }
  // public previousPage() {

  //   if (this.apiService.prev !== undefined && this.apiService.prev !== '') {
  //     this.products = [];
  //     this.apiService.sendGetRequestToUrl(this.apiService.prev).pipe(takeUntil(this.destroy$)).subscribe((res: HttpResponse<any>) => {
  //       console.log(res);
  //       this.products = res.body;
  //     })
  //   }

  // }
  // public nextPage() {
  //   if (this.apiService.next !== undefined && this.apiService.next !== '') {
  //     this.products = [];
  //     this.apiService.sendGetRequestToUrl(this.apiService.next).pipe(takeUntil(this.destroy$)).subscribe((res: HttpResponse<any>) => {
  //       console.log(res);
  //       this.products = res.body;
  //     })
  //   }
  // }
  // public lastPage() {
  //   this.products = [];
  //   this.apiService.sendGetRequestToUrl(this.apiService.last).pipe(takeUntil(this.destroy$)).subscribe((res: HttpResponse<any>) => {
  //     console.log(res);
  //     this.products = res.body;
  //   })
  // }
