import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.scss']
})
export class EntryComponent implements OnInit {
  textHeader = "Are you 21 years of age or older?";
  textSub = "Well hey there. Do you like beer? So do we. But ya kinda need to be 21 to enter.";
  hideButtons = false;

  constructor() { }

  ngOnInit() {
  }

  showDeniedCard(){
    //flip display text for header and subtitle
    this.textHeader = "We've got some bad news.";
    this.textSub = "You've got to be 21 to enter this site. Enjoy your youth and stop by when it's time.";
    //hide yes/no buttons
    console.log('made it here');
    this.hideButtons = true;
    console.log('buttton status:', this.hideButtons);
  }

}
