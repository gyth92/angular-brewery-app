import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from "@angular/common/http";

import { throwError } from 'rxjs';
import { retry, catchError, tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private SERVER_URL = "http://localhost:3000/beers";
  public first: string = "";  
  public prev: string = "";  
  public next: string = "";  
  public last: string = "";

  constructor(private httpClient: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  public sendGetRequest(){
    // Add safe, URL encoded _page and _limit parameters 

    return this.httpClient.get(this.SERVER_URL, {
      observe: "response"}).pipe(retry(3),
      catchError(this.handleError),
      tap(res => {
    }));
  }

  public sendSingleGetRequest(beerId: number) {
    return this.httpClient.get(this.SERVER_URL + '/' + beerId, {
      observe: "response"}).pipe(retry(3),
      catchError(this.handleError),
      tap(res => {
    }));
  }

  public createBeer(data){
    this.httpClient.post<any>(this.SERVER_URL, data).subscribe(data => { 
      console.log('received ok response from patch request');
    },
    error => {
      console.error('There was an error during the request');
      console.log(error);
    });
  }

  public updateBeer(beerId: number, toUpdate: object){
    console.log('sending patch request to add an item', toUpdate);

    this.httpClient.patch(this.SERVER_URL + '/' +  beerId, toUpdate).subscribe(
      res => { 
        console.log('received ok response from patch request');
      },
      error => {
        console.error('There was an error during the request');
        console.log(error);
      });
  }

  public deleteBeer(beerId: number) {
    console.log('sending delete request for:', beerId)

   this.httpClient.delete(this.SERVER_URL + '/' + beerId).subscribe(
     res => {
       console.log('deleted my dudes');
     },
     error => {
      console.error('There was an error during the request');
      console.log(error);
    });
  }
  
  // ngOnDestroy() {
  //   this.destroy$.next(true);
  //   //unsubscribe from the subject itself:
  //   this.destroy$.unsubscribe();
  // }
}