import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { EntryComponent } from './entry/entry.component';
import { BeersComponent } from './beers/beers.component';
import { AdminComponent } from './admin/admin.component';
import { UpdateBeerComponent } from './update-beer/update-beer.component';


const routes: Routes = [
  // { path: '', redirectTo: 'entry', pathMatch: 'full'},
  { path: '', component: EntryComponent },
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'beers', component: BeersComponent },
  { path: 'admin', component: AdminComponent},
  { path: 'update/:id', component: UpdateBeerComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }